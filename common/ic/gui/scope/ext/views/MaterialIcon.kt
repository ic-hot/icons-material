package ic.gui.scope.ext.views


import ic.graphics.color.Color
import ic.graphics.image.Image

import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.GuiScope
import ic.gui.scope.ext.views.image.Image


@Suppress("FunctionName")
fun GuiScope.MaterialIcon (

	icon : Image,

	tint : Color,

	horizontalAlign : HorizontalAlign = Center,
	verticalAlign 	: VerticalAlign   = Center,

) = (

	Image(
		width = 24.dp, height = 24.dp,
		horizontalAlign = horizontalAlign, verticalAlign = verticalAlign,
		image = icon,
		tint = tint
	)

)