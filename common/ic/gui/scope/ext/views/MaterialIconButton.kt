package ic.gui.scope.ext.views


import ic.struct.list.List

import ic.graphics.color.Color
import ic.graphics.image.Image

import ic.gui.align.Center
import ic.gui.align.HorizontalAlign
import ic.gui.align.VerticalAlign
import ic.gui.dim.dp.ext.dp
import ic.gui.scope.GuiScope
import ic.gui.scope.ext.views.clickable.Clickable
import ic.gui.scope.ext.views.stack.Stack


@Suppress("FunctionName")
fun GuiScope.MaterialIconButton (

	icon : Image,

	tint : Color,

	horizontalAlign : HorizontalAlign = Center,
	verticalAlign 	: VerticalAlign   = Center,

	onClick : () -> Unit

) = (

	Stack(
		width = 48.dp, height = 48.dp,
		horizontalAlign = horizontalAlign, verticalAlign = verticalAlign,
		children = List(
			MaterialIcon(
				icon = icon,
				tint = tint
			),
			Clickable(onClick = onClick)
		)
	)

)