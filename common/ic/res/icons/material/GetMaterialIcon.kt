package ic.res.icons.material


import ic.base.platform.PlatformType.Jvm.Android
import ic.base.platform.platformType
import ic.util.cache.RamCache
import ic.util.code.camelCaseToUnderscore
import ic.storage.res.getResImage

import ic.graphics.image.Image


private val cache = RamCache<String, Image>()


fun getMaterialIcon (name: String) : Image = cache.getOr(name) {

	if (platformType == Android) {

		getResImage(
			"icon_" + camelCaseToUnderscore(name) + "_24dp"
		)

	} else {

		getResImage("icons/$name")

	}

}