package ic.res.icons.material


object MaterialIcon {

	val ArrowDown  get() = getMaterialIcon("arrowDown")
	val ArrowLeft  get() = getMaterialIcon("arrowLeft")
	val ArrowRight get() = getMaterialIcon("arrowRight")
	val ArrowUp    get() = getMaterialIcon("arrowUp")
	val AxeBattle  get() = getMaterialIcon("axeBattle")

	val BagPersonalOutline get() = getMaterialIcon("bagPersonalOutline")

	val Check        get() = getMaterialIcon("check")
	val Cog          get() = getMaterialIcon("cog")
	val ContentPaste get() = getMaterialIcon("contentPaste")

	val FileDocumentOutline get() = getMaterialIcon("fileDocumentOutline")
	val FileImageOutline    get() = getMaterialIcon("fileImageOutline")
	val Folder              get() = getMaterialIcon("folder")

	val HammerWrench         get() = getMaterialIcon("hammerWrench")
	val HandBackRightOutline get() = getMaterialIcon("handBackRightOutline")

	val Link            get() = getMaterialIcon("link")
	val LockOutline     get() = getMaterialIcon("lockOutline")
	val LockOpenOutline get() = getMaterialIcon("lockOpenOutline")

	val Map get() = getMaterialIcon("map")

	val Plus get() = getMaterialIcon("plus")

	val RotateLeft  get() = getMaterialIcon("rotateLeft")
	val RotateRight get() = getMaterialIcon("rotateRight")

	val Target get() = getMaterialIcon("target")

}